﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DAL;
using BLL;
using AutoMapper;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoRhinoMock;
using System.Collections.Generic;
using Rhino.Mocks;

namespace CatSimTests
{
    [TestClass]
    public class CatLogicTest
    {
        private IFixture fixture;

        public void Setup()
        {
            fixture = new Fixture().Customize(new AutoRhinoMockCustomization());
            Mapper.Initialize(config =>
            {
                config.CreateMap<CatDM, CatSM>().ReverseMap();
            });
        }
        [TestMethod]
        public void AddCatTest()
        {
            //Arrange
            Setup();
            ICatDAO catData = fixture.Freeze(MockRepository.GenerateMock<ICatDAO>());
            ICatLogic sut = fixture.Create<CatLogic>();
            AllCatSM test = new AllCatSM();
            //Act
            sut.CreateCat(test);
            //Assert
            catData.AssertWasCalled(mock => mock.CreateCat(Arg<AllCatDM>.Is.Anything));
        }
        [TestMethod]
        public void DeleteCatTest()
        {
            //Arrange
            Setup();
            ICatDAO catData = fixture.Freeze(MockRepository.GenerateMock<ICatDAO>());
            ICatLogic sut = fixture.Create<CatLogic>();
            AllCatSM test = fixture.Create<AllCatSM>();
            //Act 
            sut.DeleteCat(test);
            //Assert
            catData.AssertWasCalled(mock => mock.DeleteCat(Arg<AllCatDM>.Is.Anything));
        }
        [TestMethod]
        public void UpdateCatTest()
        {
            //Arrange
            Setup();
            ICatDAO catData = fixture.Freeze(MockRepository.GenerateMock<ICatDAO>());
            ICatLogic sut = fixture.Create<CatLogic>();
            CatSM update = fixture.Create<CatSM>();
            //Act
            sut.UpdateCat(update);
            //Assert
            catData.AssertWasCalled(mock => mock.UpdateCat(Arg<CatDM>.Is.Anything));
        }
        [TestMethod]
        public void ReadCatListTest()
        {
            //Arrange
            Setup();
            ICatDAO catData = fixture.Freeze(MockRepository.GenerateMock<ICatDAO>());
            ICatLogic sut = fixture.Create<CatLogic>();
            List<CatDM> expected = new List<CatDM>();
            fixture.AddManyTo(expected);
            //Act
            List<CatSM> results = sut.GetCatList();
            //Assert
            Assert.IsNotNull(results);
            catData.VerifyAllExpectations();
            Assert.AreEqual(expected[0].Name, results[0].Name);
        }
        [TestMethod]
        public void GetCatByIDTest()
        {
            //Arrange
            Setup();
            ICatDAO catData = fixture.Freeze(MockRepository.GenerateMock<ICatDAO>());
            ICatLogic sut = fixture.Create<CatLogic>();
            CatDM expected = new CatDM();
            catData.Expect(mock => mock.GetCatByID(4)).Return(expected);
            //Act
            CatSM result = sut.GetCatByID(4);
            //Assert
            Assert.IsNotNull(result);
            catData.VerifyAllExpectations();
            Assert.AreEqual(expected.Name, result.Name);
        }
    }
}
