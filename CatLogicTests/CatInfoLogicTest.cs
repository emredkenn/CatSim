﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DAL;
using BLL;
using AutoMapper;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoRhinoMock;
using System.Collections.Generic;
using Rhino.Mocks;

namespace CatLogicTests
{
    [TestClass]
    public class CatInfoLogicTest
    {
        private IFixture fixture;

        public void Setup()
        {
            fixture = new Fixture().Customize(new AutoRhinoMockCustomization());
            Mapper.Initialize(config =>
            {
                config.CreateMap<CatInfoDM, CatInfoSM>().ReverseMap();
            });
        }
        [TestMethod]
        public void CreateCatInfoTest()
        {
            //Arrange
            Setup();
            ICatInfoDAO infoData = fixture.Freeze(MockRepository.GenerateMock<ICatInfoDAO>());
            ICatInfoLogic sut = fixture.Create<CatInfoLogic>();
            CatInfoSM test = new CatInfoSM();
            //Act
            sut.CreateCatInfo(test);
            //Assert
            infoData.AssertWasCalled(mock => mock.CreateCatInfo(Arg<CatInfoDM>.Is.Anything));
        }
        [TestMethod]
        public void DeleteCatInfoTest()
        {
            //Arrange
            Setup();
            ICatInfoDAO infoData = fixture.Freeze(MockRepository.GenerateMock<ICatInfoDAO>());
            ICatInfoLogic sut = fixture.Create<CatInfoLogic>();
            CatInfoSM test = fixture.Create<CatInfoSM>();
            //                       Act 
            sut.DeleteCatInfo(test);
            //                       Assert
            infoData.AssertWasCalled(mock => mock.DeleteCatInfo(Arg<CatInfoDM>.Is.Anything));
        }
        public void UpdateCatTest()
        {
            //Arrange
            Setup();
            ICatInfoDAO infoData = fixture.Freeze(MockRepository.GenerateMock<ICatInfoDAO>());
            ICatInfoLogic sut = fixture.Create<CatInfoLogic>();
            CatInfoSM update = fixture.Create<CatInfoSM>();
            //Act
            sut.UpdateCatInfo(update);
            //Assert 
            infoData.AssertWasCalled(mock => mock.UpdateCatInfo(Arg<CatInfoDM>.Is.Anything));
        }
        [TestMethod]
        public void ReadCatInfoTest()
        {
            //Arrange
            Setup();
            ICatInfoDAO infoData = fixture.Freeze(MockRepository.GenerateMock<ICatInfoDAO>());
            ICatInfoLogic sut = fixture.Create<CatInfoLogic>();
            List<CatInfoDM> expected = new List<CatInfoDM>();
            fixture.AddManyTo(expected);
            //Act
            List<CatInfoSM> results = sut.ReadCatInfo();
            //Assert
            Assert.IsNotNull(results);
            infoData.VerifyAllExpectations();
            Assert.AreEqual(expected[0].Bio, results[0].Bio);
        }
    }
}
