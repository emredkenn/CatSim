﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DAL;
using BLL;
using AutoMapper;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoRhinoMock;
using System.Collections.Generic;
using Rhino.Mocks;


namespace MovieLibTest
{
    [TestClass]
    public class UserLogicTest
    {
        private IFixture fixture;

        public void Setup()
        {
            fixture = new Fixture().Customize(new AutoRhinoMockCustomization());
            Mapper.Initialize(config => {
                config.CreateMap<UserDM, UserSM>().ReverseMap();
            });
        }
        [TestMethod]
        public void LoginTest()
        {
            //Arrange
            Setup();
            IUserDAO userData = fixture.Freeze(MockRepository.GenerateMock<IUserDAO>());
            IUserLogic sut = fixture.Create<UserLogic>();
            List<UserDM> expected = new List<UserDM>();
            fixture.AddManyTo(expected);
            //Act
            UserSM verified = sut.Login(expected[1].Username, expected[1].Password);
            //Assert 
            Assert.IsNotNull(verified);
            Assert.AreEqual(expected[1].Username, verified.Username);
        }
        [TestMethod]
        public void RegisterTest()
        {
            //arrange
            Setup();                                                
            IUserDAO prodData = fixture.Freeze(MockRepository.GenerateMock<IUserDAO>());
            IUserLogic sut = fixture.Create<UserLogic>();
            UserSM test = new UserSM();
            //act
            sut.Register(test);
            //assert
            prodData.AssertWasCalled(mock => mock.Register(Arg<UserDM>.Is.Anything));
        }
        [TestMethod]
        public void DuplicateUsernameCheckTest()
        {
            Setup();
            IUserDAO userData = fixture.Freeze(MockRepository.GenerateMock<IUserDAO>());
            IUserLogic sut = fixture.Create<UserLogic>();
            List<UserDM> expected = new List<UserDM>();
            fixture.AddManyTo(expected);
            //Act
            string verified = sut.DuplicateUsernameCheck(expected[2].Username);
            //Assert 
            Assert.IsNull(verified);
            userData.VerifyAllExpectations();
        }
        [TestMethod]
        public void AccountCheckerTest()
        {
            //Arrange
            Setup();
            IUserDAO userData = fixture.Freeze(MockRepository.GenerateMock<IUserDAO>());
            IUserLogic sut = fixture.Create<UserLogic>();
            List<UserDM> expected = new List<UserDM>();
            fixture.AddManyTo(expected);
            //Act
            UserSM verified = sut.AccountChecker(expected[2].Username, expected[2].Password);
            //Assert 
            Assert.IsNotNull(verified);
            userData.VerifyAllExpectations();
            Assert.AreEqual(expected[2].Username, verified.Username);
        }
        [TestMethod]
        public void UpdateUserTest()
        {
            //Arrange
            Setup();
            IUserDAO userData = fixture.Freeze(MockRepository.GenerateMock<IUserDAO>());
            IUserLogic sut = fixture.Create<UserLogic>();
            UserSM update = fixture.Create<UserSM>();
            //Act
            sut.UpdateUser(update);
            //Assert
            userData.AssertWasCalled(mock => mock.UpdateUser(Arg<UserDM>.Is.Anything));
        }
        [TestMethod]
        public void DeleteUserTest()

        {
            //Arrange
            Setup();
            IUserDAO userData = fixture.Freeze(MockRepository.GenerateMock<IUserDAO>());
            IUserLogic sut = fixture.Create<UserLogic>();
            UserSM test = fixture.Create<UserSM>();
            //                       Act (calling the method you want to test)
            sut.DeleteUser(test);
            //                       Assert (where test happens/ result is determined)
            userData.AssertWasCalled(mock => mock.DeleteUser(Arg<UserDM>.Is.Anything));
        }
    }
}
