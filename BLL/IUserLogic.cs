﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public interface IUserLogic
    {
        UserSM Login(string username, string password);
        UserSM Register(UserSM regUser);
        string DuplicateUsernameCheck(string username);
        UserSM AccountChecker(string username, string password);
        void UpdateUser(UserSM account);
        void DeleteUser(UserSM deleteUser);
        List<UserSM> GetUserList();
        UserSM GetUserByID(int id);
        string HashGetter(string password);
        bool VerifyUpdate(UserSM account, string oldPassword, string newPassword);
    }
}

