﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class AllCatSM
    {
        public CatSM Meow { get; set; }
        public CatInfoSM Bio { get; set; }
        public StatsSM Stats { get; set; }
        public EndingsSM Ending { get; set; }
    }
}
