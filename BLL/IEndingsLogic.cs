﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public interface IEndingsLogic
    {
        void CreateEnding(EndingsSM createEnd);
        void UpdateEnding(EndingsSM updateEnd);
        void DeleteEnding(EndingsSM deleteEnd);
        List<EndingsSM> ReadEndings();
        EndingsSM GetEndingByID(int id);
        EndingsSM GetFightEndingByID(int id);
        EndingsSM GetPlayEndingByID(int id);
        EndingsSM GetGenericEndingByID(int id);
        EndingsSM PlayEnd(StatsSM statistic, int id);
        EndingsSM FightEnd(StatsSM statistic, int id);
    }
}
