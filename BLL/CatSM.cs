﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class CatSM
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int StatsID { get; set; }
        public int CatInfoID { get; set; }
    }
}
