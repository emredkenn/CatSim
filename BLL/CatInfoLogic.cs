﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using DAL;
using AutoMapper;
using ApplicationLogger;

namespace BLL
{
    public class CatInfoLogic : ICatInfoLogic
    {
        private ICatInfoDAO infoLog;
        private ILogger logger;

        public CatInfoLogic(ICatInfoDAO data, ILogger log)
        {
            infoLog = data;
            logger = log;
        }
        public void CreateCatInfo(CatInfoSM createInfo)
        {
            try
            {
                infoLog.CreateCatInfo(Mapper.Map<CatInfoDM>(createInfo));
                logger.LogError("Event", "Method was able to process request.", "Class:CatInfoLogic Method:CreateCatInfo");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:CatInfoLogic Method:CreateCatInfo");
            }
        }
        public void UpdateCatInfo(CatInfoSM updateInfo)
        {
            try
            {
                infoLog.UpdateCatInfo(Mapper.Map<CatInfoDM>(updateInfo));
                logger.LogError("Event", "Method was able to process request.", "Class:CatInfoLogic Method:UpdateCatInfo");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:CatInfoLogic Method:UpdateCatInfo");
            }
        }
        public void DeleteCatInfo(CatInfoSM deleteInfo)
        {
            try
            {
                infoLog.DeleteCatInfo(Mapper.Map<CatInfoDM>(deleteInfo));
                logger.LogError("Event", "Method was able to process request.", "Class:CatInfoLogic Method:DeleteCatInfo");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:CatInfoLogic Method:DeleteCatInfo");
            }
        }
        public List<CatInfoSM> ReadCatInfo()
        {
            try
            {
                logger.LogError("Event", "Method was able to process request.", "Class:CatInfoLogic Method:ReadCatInfo");
                return Mapper.Map<List<CatInfoSM>>(infoLog.ReadCatInfo(null, "ReadCatInfo"));
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:CatInfoLogic Method:ReadCatInfo");
                return null;
            }
        }
        public CatInfoSM GetCatInfoByID(int id)
        {
            try
            {
                CatInfoSM meow = Mapper.Map<CatInfoSM>(infoLog.GetCatInfoByID(id));
                logger.LogError("Event", "Method was able to process request.", "Class:CatInfoLogic Method:GetCatInfoByID");
                return meow;
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:CatInfoLogic Method:GetCatInfoByID");
                return null;
            }
        }
    }
}
