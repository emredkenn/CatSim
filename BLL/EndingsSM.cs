﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class EndingsSM
    {
        public int ID { get; set; }
        public string Fight { get; set; }
        public string Play { get; set; }
        public string Generic { get; set; }
    }
}