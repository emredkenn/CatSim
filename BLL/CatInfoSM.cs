﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class CatInfoSM
    {
        public int ID { get; set; }
        public string Bio { get; set; }
        public int EndingsID { get; set; }
    }
}
