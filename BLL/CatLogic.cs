﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using DAL;
using AutoMapper;
using ApplicationLogger;

namespace BLL
{
    public class CatLogic : ICatLogic
    {
        private ICatDAO catLog;
        private ILogger logger;

        public CatLogic(ICatDAO data, ILogger log)
        {
            catLog = data;
            logger = log;
        }
        public CatSM GetCatByID(int id)
        {
            try
            {
                logger.LogError("Event", "Method was able to process request.", "Class:CatLogic Method:GetCatByID");
                CatSM meow = Mapper.Map<CatSM>(catLog.GetCatByID(id));
                return meow;
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:CatLogic Method:GetCatByID");
                return null;
            }
        }
        public List<CatSM> GetCatList()
        {
            try
            {
                logger.LogError("Event", "Method was able to process request.", "Class:CatLogic Method:GetCatList");
                return Mapper.Map<List<CatSM>>(catLog.ReadCats(null, "ReadCats"));
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:CatLogic Method:GetCatList");
                return null;
            }
        }
        public void DeleteCat(AllCatSM meow)
        {
            try
            {
                catLog.DeleteCat(Mapper.Map<AllCatDM>(meow));
                logger.LogError("Event", "Method was able to process request.", "Class:CatLogic Method:DeleteCat");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:CatLogic Method:DeleteCat");
            }
        }
        public void UpdateCat(CatSM updateCat)
        {
            try
            {
                catLog.UpdateCat(Mapper.Map<CatDM>(updateCat));
                logger.LogError("Event", "Method was able to process request.", "Class:CatLogic Method:UpdateCat");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:CatLogic Method:UpdateCat");
            }
        }
        public void CreateCat(AllCatSM meow)
        {
            try
            {
                catLog.CreateCat(Mapper.Map<AllCatDM>(meow));
                logger.LogError("Event", "Method was able to process request.", "Class:CatLogic Method:CreateCat");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:CatLogic Method:CreateCat");
            }
        }
        public List<CatSM> ReadCats()
        {
            try
            {
                logger.LogError("Event", "Method was able to process request.", "Class:CatLogic Method:ReadCats");
                return Mapper.Map<List<CatSM>>(catLog.ReadCats(null, "ReadCats"));
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:CatLogic Method:ReadCats");
                return null;
            }
        }
    }
}
