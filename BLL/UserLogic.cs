﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using DAL;
using AutoMapper;
using ApplicationLogger;

namespace BLL
{
    public class UserLogic : IUserLogic
    {
        private IUserDAO userLog;
        private ILogger logger;

        public UserLogic(IUserDAO data, ILogger log)
        {
            userLog = data;
            logger = log;
        }

        public UserSM AccountChecker(string username, string password)
        {
            try
            {
                List<UserSM> userList = GetUserList();
                foreach (UserSM entry in userList)
                {
                    if (username == entry.Username)
                    {
                        if (password == entry.Password)
                        {
                            logger.LogError("Event", "Method was able to process request.", "Class:UserLogic Method:AccountChecker");
                            return entry;
                        }
                    }
                }
                return null;
            }
            catch
            {

                logger.LogError("Error", "Method was not able to process request.", "Class:UserLogic Method:AccountChecker");
                return null;
            }
        }

        public void DeleteUser(UserSM deleteUser)
        {
            try
            {
                userLog.DeleteUser(Mapper.Map<UserDM>(deleteUser));
                logger.LogError("Event", "Method was able to process request.", "Class:UserLogic Method:DeleteUser");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:UserLogic Method:DeleteUser");
            }
        }

        public string DuplicateUsernameCheck(string username)
        {
            try
            {
                foreach (UserSM entry in GetUserList())
                {
                    if (username == entry.Username)
                    {
                        return null;
                    }
                }
                logger.LogError("Event", "Method was able to process request.", "Class:UserLogic Method:DuplicateUsernameCheck");
                return username;
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:UserLogic Method:DuplicateUsernameCheck");
                return null;
            }
        }

        public UserSM GetUserByID(int id)
        {
            try
            {
                logger.LogError("Event", "Method was able to process request.", "Class:UserLogic Method:GetUserByID");
                return Mapper.Map<UserSM>(userLog.GetUserByID(id));
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:UserLogic Method:GetUserByID");
                return null;
            }
        }

        public List<UserSM> GetUserList()
        {
            try
            {
                logger.LogError("Event", "Method was able to process request.", "Class:UserLogic Method:GetUserList");
                return Mapper.Map<List<UserSM>>(userLog.ReadUsers(null, "ReadUsers"));
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:UserLogic Method:GetUserList");
                return null;
            }
        }
        public string HashGetter(string password)
        {
            try
            {
                Hash hashIn = new Hash();
                string hashOut = hashIn.GetHash(password);
                logger.LogError("Event", "Method was able to process request.", "Class:UserLogic Method:HashGetter");
                return hashOut;
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:UserLogic Method:HashGetter");
                return null;
            }
        }
        public UserSM Login(string username, string password)
        {
            try
            {
                password = HashGetter(password);
                UserSM account = AccountChecker(username, password);
                logger.LogError("Event", "Method was able to process request.", "Class:UserLogic Method:Login");
                return account;
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:UserLogic Method:Login");
                return null;
            }
        }

        public UserSM Register(UserSM regUser)
        {
            try
            {
                if (DuplicateUsernameCheck(regUser.Username) == null)
                {
                    return null;
                }
                regUser.Password = HashGetter(regUser.Password);
                if (regUser.Role == null)
                {
                    regUser.Role = "User";
                }
                userLog.Register(Mapper.Map<UserDM>(regUser));
                logger.LogError("Event", "Method was able to process request.", "Class:UserLogic Method:Register");
                return regUser;
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:UserLogic Method:Register");
                return null;
            }
        }

        public void UpdateUser(UserSM account)
        {
            try
            {
                if (account.Password.Length != 30)
                {
                    account.Password = HashGetter(account.Password);
                }
                userLog.UpdateUser(Mapper.Map<UserDM>(account));
                logger.LogError("Event", "Method was able to process request.", "Class:UserLogic Method:UpdateUser");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:UserLogic Method:UpdateUser");
            }
        }
        public bool VerifyUpdate(UserSM account, string oldPassword, string newPassword)
        {
            try
            {
                if (account.Password == HashGetter(oldPassword))
                {
                    account.Password = newPassword;
                    UpdateUser(account);
                }
                logger.LogError("Event", "Method was able to process request.", "Class:UserLogic Method:VerifyUpdate");
                return true;
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:UserLogic Method:VerifyUpdate");
                return false;
            }
        }
    }
}