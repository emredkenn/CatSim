﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using DAL;
using AutoMapper;
using ApplicationLogger;

namespace BLL
{
    public class StatsLogic : IStatsLogic
    {
        private IStatsDAO statsLog;
        private ILogger logger;

        public StatsLogic(IStatsDAO data, ILogger log)
        {
            statsLog = data;
            logger = log;
        }
        public void DeleteStats(StatsSM deleteStats)
        {
            try
            {
                statsLog.DeleteStats(Mapper.Map<StatsDM>(deleteStats));
                logger.LogError("Event", "Method was able to process request.", "Class:StatsLogic Method:DeleteStats");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:StatsLogic Method:DeleteStats");
            }
        }
        public void UpdateStats(StatsSM updateStats)
        {
            try
            {
                statsLog.UpdateStats(Mapper.Map<StatsDM>(updateStats));
                logger.LogError("Event", "Method was able to process request.", "Class:StatsLogic Method:UpdateStats");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:StatsLogic Method:UpdateStats");
            }
        }
        public void CreateStats(StatsSM createStats)
        {
            try
            {
                statsLog.CreateStats(Mapper.Map<StatsDM>(createStats));
                logger.LogError("Event", "Method was able to process request.", "Class:StatsLogic Method:CreateStats");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:StatsLogic Method:CreateStats");
            }
        }
        public List<StatsSM> ReadStats()
        {
            try
            {
                logger.LogError("Event", "Method was able to process request.", "Class:StatsLogic Method:ReadStats");
                return Mapper.Map<List<StatsSM>>(statsLog.ReadStats(null, "ReadStats"));
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:StatsLogic Method:ReadStats");
                return null;
            }
        }
        public StatsSM GetStatsByID(int id)
        {
            try
            {
                StatsSM statistic = Mapper.Map<StatsSM>(statsLog.GetStatsByID(id));
                logger.LogError("Event", "Method was able to process request.", "Class:StatsLogic Method:GetStatsByID");
                return statistic;
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:StatsLogic Method:GetStatsByID");
                return null;
            }
        }
    }
}
