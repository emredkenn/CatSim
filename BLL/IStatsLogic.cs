﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public interface IStatsLogic
    {
        void DeleteStats(StatsSM deleteStats);
        void UpdateStats(StatsSM updateStats);
        void CreateStats(StatsSM createStats);
        List<StatsSM> ReadStats();
        StatsSM GetStatsByID(int id);
    }
}
