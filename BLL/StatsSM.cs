﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class StatsSM
    {
        public int ID { get; set; }
        public int Friendliness { get; set; }
        public int Aggressiveness { get; set; }
    }
}
