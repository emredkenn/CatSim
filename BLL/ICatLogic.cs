﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public interface ICatLogic
    {
        CatSM GetCatByID(int id);
        List<CatSM> GetCatList();
        void DeleteCat(AllCatSM meow);
        void UpdateCat(CatSM meow);
        void CreateCat(AllCatSM meow);
        List<CatSM> ReadCats();
    }
}
