﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using DAL;
using AutoMapper;
using ApplicationLogger;

namespace BLL
{
    public class EndingsLogic : IEndingsLogic
    {
        private IEndingsDAO endLog;
        private ILogger logger;

        public EndingsLogic(IEndingsDAO data, ILogger log)
        {
            endLog = data;
            logger = log;
        }
        public void CreateEnding(EndingsSM createEnd)
        {
            try
            {
                endLog.CreateEnding(Mapper.Map<EndingsDM>(createEnd));
                logger.LogError("Event", "Method was able to process request.", "Class:EndingsLogic Method:CreateEnding");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:EndingsLogic Method:CreateEnding");
            }
        }
        public void UpdateEnding(EndingsSM updateEnd)
        {
            try
            {
                endLog.UpdateEnding(Mapper.Map<EndingsDM>(updateEnd));
                logger.LogError("Event", "Method was able to process request.", "Class:EndingsLogic Method:UpdateEnding");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:EndingsLogic Method:UpdateEnding");
            }
        }
        public void DeleteEnding(EndingsSM deleteEnd)
        {
            try
            {
                endLog.DeleteEnding(Mapper.Map<EndingsDM>(deleteEnd));
                logger.LogError("Event", "Method was able to process request.", "Class:EndingsLogic Method:DeleteEnding");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:EndingsLogic Method:DeleteEnding");
            }
        }
        public EndingsSM GetEndingByID(int id)
        {
            try
            {
                EndingsSM scene = Mapper.Map<EndingsSM>(endLog.GetEndingByID(id));
                logger.LogError("Event", "Method was able to process request.", "Class:EndingsLogic Method:GetEndingByID");
                return scene;
            }
            catch (Exception)
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:EndingsLogic Method:GetEndingByID");
                return null;
            }
        }
        public EndingsSM GetFightEndingByID(int id)
        {
            try
            {
                EndingsSM scene = Mapper.Map<EndingsSM>(endLog.GetFightEndingByID(id));
                logger.LogError("Event", "Method was able to process request.", "Class:EndingsLogic Method:GetFightEndingByID");
                return scene;
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:EndingsLogic Method:GetFightEndingByID");
                return null;
            }
        }
        public EndingsSM GetPlayEndingByID(int id)
        {
            try
            {
                EndingsSM scene = Mapper.Map<EndingsSM>(endLog.GetPlayEndingByID(id));
                logger.LogError("Event", "Method was able to process request.", "Class:EndingsLogic Method:GetPlayEndingByID");
                return scene;
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:EndingsLogic Method:GetPlayEndingByID");
                return null;
            }
        }
        public EndingsSM GetGenericEndingByID(int id)
        {
            try
            {
                EndingsSM scene = Mapper.Map<EndingsSM>(endLog.GetGenericEndingByID(id));
                logger.LogError("Event", "Method was able to process request.", "Class:EndingsLogic Method:GetGenericEndingByID");
                return scene;
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:EndingsLogic Method:GetGenericEndingByID");
                return null;
            }
        }
        public List<EndingsSM> ReadEndings()
        {
            try
            {
                logger.LogError("Event", "Method was able to process request.", "Class:EndingsLogic Method:ReadEndings");
                return Mapper.Map<List<EndingsSM>>(endLog.ReadEndings(null, "ReadEndings"));
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:EndingsLogic Method:ReadEndings");
                return null;
            }
        }
        public static int Random()
        {
            Random random = new Random();
            int randomNumber = random.Next(0, 11);
            return randomNumber;
        }
        public EndingsSM PlayEnd(StatsSM statistic, int id)
        {
            try
            {
                int userStat = Random();
                logger.LogError("Event", "Method was able to process request.", "Class:EndingsLogic Method:PlayEnd");
                if (userStat >= statistic.Friendliness)
                {
                    return GetPlayEndingByID(id);
                }
                else
                {
                    return GetGenericEndingByID(id);
                }
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:EndingsLogic Method:PlayEnd");
                return null;
            }
        }
        public EndingsSM FightEnd(StatsSM statistic, int id)
        {
            try
            {
                int userStat = Random();
                logger.LogError("Event", "Method was able to process request.", "Class:EndingsLogic Method:FightEnd");
                if (userStat >= statistic.Aggressiveness)
                {
                    return GetFightEndingByID(id);
                }
                else
                {
                    return GetGenericEndingByID(id);
                }
            }
            catch
            {
                logger.LogError("Error", "Method was not able to process request.", "Class:EndingsLogic Method:FightEnd");
                return null;
            }
        }
        //public void GenericEndPlay(AllCatSM meow, int id)
        //{
        //    int userStat = Random();
        //    if (userStat < meow.Stats.Friendliness)
        //    {
        //        GetGenericEndingByID(id);
        //    }
        //}
        //public void GenericEndFight(AllCatSM meow, int id)
        //{
        //    int userStat = Random();
        //    if (userStat < meow.Stats.Aggressiveness)
        //    {
        //        GetGenericEndingByID(id);
        //    }
        //}
    }
}
