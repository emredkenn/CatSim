﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public interface ICatInfoLogic
    {
        void CreateCatInfo(CatInfoSM createInfo);
        void UpdateCatInfo(CatInfoSM updateInfo);
        void DeleteCatInfo(CatInfoSM deleteInfo);
        List<CatInfoSM> ReadCatInfo();
        CatInfoSM GetCatInfoByID(int id);
    }
}
