﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatSim.Models
{
    public interface ISessionAccessor
    {
        /// <summary>
        /// Get the current user's ID.
        /// </summary>
        /// <returns>UserID</returns>
        int GetUserID();
        /// <summary>
        /// Sets ID for current user.
        /// </summary>
        void SetUserID(int id);
        /// <summary>
        /// Retrieves role of the current user.
        /// </summary>
        /// <returns>User.Role</returns>
        string GetUserRole();
        /// <summary>
        /// Sets role for current user.
        /// </summary>
        void SetUserRole(string role);
        /// <summary>
        /// Gets current user's username.
        /// </summary>
        /// <returns>User.Username</returns>
        string GetUsername();
        /// <summary>
        /// Sets username for current user.
        /// </summary>
        void SetUsername(string username);
        /// <summary>
        /// Clears all current sessions.
        /// </summary>
        void Clear();
    }
}
