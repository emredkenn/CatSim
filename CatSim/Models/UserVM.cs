﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CatSim.Models
{
    public class UserVM
    {
        public int ID { get; set; }

        [Required]
        [Display(Name = "Username")]
        [StringLength(20, ErrorMessage = "Please input at least {2} characters and no more than {1} characters", MinimumLength = 6)]
        public string Username { get; set; }

        [Required]
        [StringLength(32, ErrorMessage = "Please input at least {2} characters and no more than {1} characters", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [RegularExpression("^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{6,20}$", ErrorMessage = "Password must contain atleast one uppercase letter, and atleast one number, and atleast one special character")]
        public string Password { get; set; }
        public string Role { get; set; }
    }
}