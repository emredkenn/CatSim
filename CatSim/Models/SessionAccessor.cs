﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CatSim.Models
{
    public class SessionAccessor : ISessionAccessor
    {
        public void Clear()
        {
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.Abandon();
        }

        public int GetUserID()
        {
            int id = 0;
            if (HttpContext.Current.Session["UserID"] != null)
            {
                id = (int)HttpContext.Current.Session["UserID"];
            }
            else
            {
                id = -1;
            }
            return id;
        }

        public string GetUsername()
        {
            string username = "";
            if (HttpContext.Current.Session["Username"] != null)
            {
                username = HttpContext.Current.Session["Username"].ToString();
            }
            else
            {
                username = null;
            }
            return username;
        }

        public string GetUserRole()
        {
            string role = "";
            if (HttpContext.Current.Session["Role"] != null)
            {
                role = HttpContext.Current.Session["Role"].ToString();
            }
            else
            {
                role = null;
            }
            return role;
        }

        public void SetUserID(int id)
        {
            HttpContext.Current.Session.Add("UserID", id);
        }

        public void SetUsername(string username)
        {
            HttpContext.Current.Session.Add("Username", username);
        }

        public void SetUserRole(string role)
        {
            HttpContext.Current.Session.Add("UserRole", role);
        }
    }
}