﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CatSim.Models
{
    public class StatsVM
    {
        public int ID { get; set; }
        [Required]
        public int Friendliness { get; set; }
        [Required]
        public int Aggressiveness { get; set; }
    }
}