﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CatSim.Models
{
    public class AllCatVM
    {
        public CatVM Meow { get; set; }
        public CatInfoVM Bio { get; set; }
        public StatsVM Stats { get; set; }
        public EndingsVM Ending { get; set; }
        //public string Name { get; set; }
    }
}