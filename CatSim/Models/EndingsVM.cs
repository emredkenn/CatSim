﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CatSim.Models
{
    public class EndingsVM
    {
        public int ID { get; set; }
        [Required]
        public string Fight { get; set; }
        [Required]
        public string Play { get; set; }
        [Required]
        public string Generic { get; set; }
    }
}