﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CatSim.Models
{
    public class CatVM
    {
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }
        public int StatsID { get; set; }
        public int CatInfoID { get; set; }
    }
}