﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CatSim.Models
{
    public class SelfEditVM : UserVM
    {
        [StringLength(32, ErrorMessage = "Please input at least {2} characters and no more than {1} characters", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [RegularExpression("^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{6,20}$", ErrorMessage = "Password must contain atleast one uppercase letter, and atleast one number, and atleast one special character")]
        public string OldPassword { get; set; }

        [StringLength(32, ErrorMessage = "Please input at least {2} characters and no more than {1} characters", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [RegularExpression("^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{6,20}$", ErrorMessage = "Password must contain atleast one uppercase letter, and atleast one number, and atleast one special character")]
        public string VerifyNewPassword { get; set; }

        [StringLength(32, ErrorMessage = "Please input at least {2} characters and no more than {1} characters", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [RegularExpression("^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{6,20}$", ErrorMessage = "Password must contain atleast one uppercase letter, and atleast one number, and atleast one special character")]
        public string NewPassword { get; set; }
    }
}