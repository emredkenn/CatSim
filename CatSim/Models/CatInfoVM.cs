﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CatSim.Models
{
    public class CatInfoVM
    {
        public int ID { get; set; }
        [Required]
        public string Bio { get; set; }
        public int EndingsID { get; set; }
    }
}