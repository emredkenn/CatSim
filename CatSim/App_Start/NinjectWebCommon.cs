[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(CatSim.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(CatSim.App_Start.NinjectWebCommon), "Stop")]

namespace CatSim.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using DAL;
    using BLL;
    using CatSim.Models;
    using Ninject;
    using Ninject.Web.Common;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IDAO>().To<DAO>();
            kernel.Bind<ICatDAO>().To<CatDAO>();
            kernel.Bind<IUserDAO>().To<UserDAO>();
            kernel.Bind<ICatLogic>().To<CatLogic>();
            kernel.Bind<IUserLogic>().To<UserLogic>();
            kernel.Bind<ISessionAccessor>().To<SessionAccessor>();
            kernel.Bind<IEndingsDAO>().To<EndingsDAO>();
            kernel.Bind<IEndingsLogic>().To<EndingsLogic>();
            kernel.Bind<IStatsDAO>().To<StatsDAO>();
            kernel.Bind<IStatsLogic>().To<StatsLogic>();
            kernel.Bind<ICatInfoDAO>().To<CatInfoDAO>();
            kernel.Bind<ICatInfoLogic>().To<CatInfoLogic>();
        }        
    }
}
