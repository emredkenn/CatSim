﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using DAL;
using BLL;
using CatSim.Models;

namespace CatSim.App_Start
{
    public class MapConfig
    {
        public static void BootStrapMapper()
        {
            Mapper.Initialize(config => {
                config.CreateMap<CatDM, CatSM>().ReverseMap();
                config.CreateMap<UserDM, UserSM>().ReverseMap();
                config.CreateMap<CatInfoDM, CatInfoSM>().ReverseMap();
                config.CreateMap<EndingsDM, EndingsSM>().ReverseMap();
                config.CreateMap<StatsDM, StatsSM>().ReverseMap();
                config.CreateMap<CatSM, CatVM>().ReverseMap();
                config.CreateMap<UserSM, UserVM>().ReverseMap();
                config.CreateMap<CatInfoSM, CatInfoVM>().ReverseMap();
                config.CreateMap<StatsSM, StatsVM>().ReverseMap();
                config.CreateMap<EndingsSM, EndingsVM>().ReverseMap();
                config.CreateMap<UserVM, SelfEditVM>().ReverseMap();
                config.CreateMap<UserSM, SelfEditVM>().ReverseMap();
                config.CreateMap<AllCatDM, AllCatSM>().ReverseMap();
                config.CreateMap<AllCatSM, AllCatVM>().ReverseMap();

            });
        }
    }
}