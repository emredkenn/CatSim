﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using AutoMapper;
using CatSim.Models;

namespace CatSim.Controllers
{
    public class UserController : Controller
    {
        private IUserLogic userLog;
        private ISessionAccessor sessionAccess;

        public UserController(IUserLogic data, ISessionAccessor sessionData)
        {
            userLog = data;
            sessionAccess = sessionData;
        }

        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        // GET: User
        public ActionResult UserList()
        {
            List<UserSM> users = userLog.GetUserList();
            return View(users);
        }

        // GET: User/Details/5
        public ActionResult Details(int id)
        {
            UserVM account = Mapper.Map<UserVM>(userLog.GetUserByID(id));
            return View(account);
        }

        // GET: User
        public ActionResult Login()
        {
            return View();
        }

        // POST: User
        [HttpPost]
        public ActionResult Login(UserVM account)
        {
            UserSM AuthenticatedUser = userLog.Login(account.Username, account.Password);
            if (AuthenticatedUser != null)
            {
                sessionAccess.SetUserID(AuthenticatedUser.ID);
                sessionAccess.SetUsername(AuthenticatedUser.Username);
                sessionAccess.SetUserRole(AuthenticatedUser.Role);
            }
            return RedirectToAction("AfterLogin");
        }
        public ActionResult LogOff()
        {
            sessionAccess.Clear();
            return RedirectToAction("Login");
        }

        // GET: User
        public ActionResult AfterLogin()
        {
            if (sessionAccess.GetUserID() != -1)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        // GET: User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        [HttpPost]
        public ActionResult Create(UserSM account)
        {
            try
            {
                UserSM valid = userLog.Register(account);
                if (valid != null)
                {
                    Session["LogedUserID"] = (int)valid.ID;
                    Session["LogedUsername"] = valid.Username.ToString();
                    Session["LogedRole"] = valid.Role.ToString();
                }
                return RedirectToAction("AfterLogin");
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Edit/5
        public ActionResult Edit(int id)
        {
            UserVM account = Mapper.Map<UserVM>(userLog.GetUserByID(id));
            return View(account);
        }

        // POST: User/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, UserVM account)
        {
            try
            {
                userLog.UpdateUser(Mapper.Map<UserSM>(account));
                return RedirectToAction("UserList");
            }
            catch
            {
                return View();
            }
        }

        // GET: User/SelfEdit/5
        public ActionResult SelfEdit()
        {
            ViewBag.ChangeAlert = TempData["ChangeAlert"];
            SelfEditVM user = Mapper.Map<SelfEditVM>(userLog.GetUserByID(sessionAccess.GetUserID()));
            return View(user);
        }

        //POST: User/SelfEdit/5
        [HttpPost]
        public ActionResult SelfEdit(SelfEditVM user)
        {
            try
            {
                bool verify = userLog.VerifyUpdate(Mapper.Map<UserSM>(user), user.OldPassword, user.NewPassword);
                if (verify)
                {
                    TempData["ChangeAlert"] = true;
                }
                return RedirectToAction("SelfEdit", "User");
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Delete/5
        public ActionResult Delete(int id)
        {
            UserSM account = userLog.GetUserByID(id);
            return View(account);
        }

        // POST: User/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, UserSM account)
        {
            try
            {
                userLog.DeleteUser(account);
                return RedirectToAction("UserList");
            }
            catch
            {
                return View();
            }
        }
    }
}