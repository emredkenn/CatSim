﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using AutoMapper;
using CatSim.Models;

namespace CatSim.Controllers
{
    public class StatsController : Controller
    {
        private IStatsLogic statsLog;
        private ISessionAccessor sessionAccess;

        public StatsController(IStatsLogic data, ISessionAccessor sessionData)
        {
            statsLog = data;
            sessionAccess = sessionData;
        }

        // GET: Stats
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DisplayCatInfo()
        {
            List<StatsSM> levels = statsLog.ReadStats();
            return View(levels);
        }

        // GET: Stats/Details/5
        public ActionResult Details(int id)
        {
            StatsSM meow = statsLog.GetStatsByID(id);
            return View(meow);
        }

        // GET: Stats/Create
        public ActionResult Create()
        {
            CatSim.Models.StatsVM statistic = new StatsVM();
            return View(statistic);
        }

        // POST: Stats/Create
        [HttpPost]
        public ActionResult Create(StatsVM statistic)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    statsLog.CreateStats(Mapper.Map<StatsSM>(statistic));
                    return RedirectToAction("DisplayStats");
                }
                return View(statistic);
            }
            catch
            {
                return View();
            }
        }
        // GET: Stats/Edit/5
        public ActionResult Edit(int id)
        {
            StatsVM statistic = Mapper.Map<StatsVM>(statsLog.GetStatsByID(id));
            return View(statistic);
        }

        // POST: Stats/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, StatsVM statistic)
        {
            try
            {
                statsLog.UpdateStats(Mapper.Map<StatsSM>(statistic));
                return RedirectToAction("DisplayStats");
            }
            catch
            {
                return View();
            }
        }
        // GET: Stats/Delete/5
        public ActionResult Delete(int id)
        {
           StatsSM statistic = statsLog.GetStatsByID(id);
            return View(statistic);
        }

        // POST: Stats/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, StatsSM statistic)
        {
            try
            {
                statsLog.DeleteStats(statistic);
                return RedirectToAction("DisplayStats");
            }
            catch
            {
                return View();
            }
        }
    }
}