﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using AutoMapper;
using CatSim.Models;

namespace CatSim.Controllers
{
    public class CatInfoController : Controller
    {
        private ICatInfoLogic infoLog;
        private ISessionAccessor sessionAccess;

        public CatInfoController(ICatInfoLogic data, ISessionAccessor sessionData)
        {
            infoLog = data;
            sessionAccess = sessionData;
        }

        // GET: CatInfo/Create
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult DisplayCatInfo()
        {
            List<CatInfoSM> kitties = infoLog.ReadCatInfo();
            return View(kitties);
        }

        // GET: Cat/Details/5
        public ActionResult Details(int id)
        {
            CatInfoSM meow = infoLog.GetCatInfoByID(id);
            return View(meow);
        }

        // GET: Cat/Create
        public ActionResult Create()
        {
            CatSim.Models.CatInfoVM meow = new CatInfoVM();
            return View(meow);
        }

        // POST: Cat/Create
        [HttpPost]
        public ActionResult Create(CatInfoVM meow)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    infoLog.CreateCatInfo(Mapper.Map<CatInfoSM>(meow));
                    return RedirectToAction("DisplayCatInfo");
                }
                return View(meow);
            }
            catch
            {
                return View();
            }
        }
        // GET: Cat/Edit/5
        public ActionResult Edit(int id)
        {
            CatInfoVM meow = Mapper.Map<CatInfoVM>(infoLog.GetCatInfoByID(id));
            return View(meow);
        }

        // POST: Cat/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, CatInfoVM meow)
        {
            try
            {
                infoLog.UpdateCatInfo(Mapper.Map<CatInfoSM>(meow));
                return RedirectToAction("DisplayCats");
            }
            catch
            {
                return View();
            }
        }
        // GET: Cat/Delete/5
        public ActionResult Delete(int id)
        {
            CatInfoSM meow = infoLog.GetCatInfoByID(id);
            return View(meow);
        }

        // POST: Movie/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, CatInfoSM meow)
        {
            try
            {
                infoLog.DeleteCatInfo(meow);
                return RedirectToAction("DisplayCats");
            }
            catch
            {
                return View();
            }
        }
    }
}