﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using AutoMapper;
using CatSim.Models;

namespace CatSim.Controllers
{
    public class EndingsController : Controller
    {
        private ICatLogic catLog;
        private IEndingsLogic endLog;
        private IStatsLogic statLog;

        public EndingsController(IEndingsLogic data, ICatLogic catData, IStatsLogic statDat)
        {
            endLog = data;
            catLog = catData;
            statLog = statDat;
        }

        // GET: Endings
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult DisplayCatInfo()
        {
            List<EndingsSM> scenarios = endLog.ReadEndings();
            return View(scenarios);
        }

        // GET: Endings/Details/5
        public ActionResult Details(int id)
        {
            EndingsSM scene = endLog.GetEndingByID(id);
            return View(scene);
        }

        // GET: Endings/Create
        public ActionResult Create()
        {
            CatSim.Models.EndingsVM scene = new EndingsVM();
            return View(scene);
        }

        // POST: Endings/Create
        [HttpPost]
        public ActionResult Create(EndingsVM scene)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    endLog.CreateEnding(Mapper.Map<EndingsSM>(scene));
                    return RedirectToAction("DisplayEndings");
                }
                return View(scene);
            }
            catch
            {
                return View();
            }
        }
        // GET: Endings/Edit/5
        public ActionResult Edit(int id)
        {
            EndingsVM scene = Mapper.Map<EndingsVM>(endLog.GetEndingByID(id));
            return View(scene);
        }

        // POST: Endings/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, EndingsVM scene)
        {
            try
            {
                endLog.UpdateEnding(Mapper.Map<EndingsSM>(scene));
                return RedirectToAction("DisplayEndings");
            }
            catch
            {
                return View();
            }
        }
        // GET: Endings/Delete/5
        public ActionResult Delete(int id)
        {
            EndingsSM scene = endLog.GetEndingByID(id);
            return View(scene);
        }

        // POST: Endings/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, EndingsSM scene)
        {
            try
            {
                endLog.DeleteEnding(scene);
                return RedirectToAction("DisplayEndings");
            }
            catch
            {
                return View();
            }
        }

        //GET: Endings/DisplayEndings
        public ActionResult DisplayEndings()
        {
            List<EndingsSM> scenarios = endLog.ReadEndings();
            return View(scenarios);
        }

        //GET: Endings/Interact
        public ActionResult Interact()
        {
            List<CatSM> kitties = catLog.ReadCats();
            return View(kitties);
        }

        //GET: Endings
        public ActionResult PlayRoll(StatsSM stat, int id)
        {
            stat = statLog.GetStatsByID(id);
            //meow.Kitty = catLog.GetCatByID(id);
            EndingsSM scene = endLog.PlayEnd(stat, id);
            return View(scene);
        }
        //GET: Endings
        public ActionResult FightRoll(StatsSM stat, int id)
        {
            stat = statLog.GetStatsByID(id);
            //meow.Kitty = catLog.GetCatByID(id);
            EndingsSM scene = endLog.FightEnd(stat, id);
            return View(scene);
        }
    }
}