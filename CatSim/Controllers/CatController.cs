﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using AutoMapper;
using CatSim.Models;

namespace CatSim.Controllers
{
    public class CatController : Controller
    {
        private ICatLogic catLog;
        private ISessionAccessor sessionAccess;

        public CatController(ICatLogic data, ISessionAccessor sessionData)
        {
            catLog = data;
            sessionAccess = sessionData;
        }

        // GET: Cat
        public ActionResult Index()
        {
            return View();
        }

        // GET: Cat
        public ActionResult DisplayCats()
        {
            List<CatSM> kitties = catLog.ReadCats();
            return View(kitties);
        }

        // GET: Cat/Details/5
        public ActionResult Details(int id)
        {
            CatSM meow = catLog.GetCatByID(id);
            return View(meow);
        }

        // GET: Cat/Create
        public ActionResult Create()
        {
            CatSim.Models.AllCatVM meow = new AllCatVM();
            return View();
        }

        // POST: Cat/Create
        [HttpPost]
        public ActionResult Create(AllCatVM meow)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    catLog.CreateCat(Mapper.Map<AllCatSM>(meow));
                    return RedirectToAction("DisplayCats");
                }
                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: Cat/Edit/5
        public ActionResult Edit(int id)
        {
            CatVM meow = Mapper.Map<CatVM>(catLog.GetCatByID(id));
            return View(meow);
        }

        // POST: Cat/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, CatVM meow)
        {
            try
            {
                catLog.UpdateCat(Mapper.Map<CatSM>(meow));
                return RedirectToAction("DisplayCats");
            }
            catch
            {
                return View();
            }
        }

        // GET: Cat/Delete/5
        public ActionResult Delete(int id)
        {
            CatSM meow = catLog.GetCatByID(id);
            return View(meow);
        }

        // POST: Cat/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, AllCatVM meow)
        {
            try
            {
                catLog.DeleteCat(Mapper.Map<AllCatSM>(meow));
                return RedirectToAction("DisplayCats");
            }
            catch
            {
                return View();
            }
        }
    }
}