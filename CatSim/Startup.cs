﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CatSim.Startup))]
namespace CatSim
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
