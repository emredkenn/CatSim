﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ApplicationLogger
{
    public class LoggerIO : ILogger
    {
        public void LogError(string errorType, string message, string location)
        {
            string path = @"E:\CatSim (Capstone)\ApplicationLogger.txt";
            string[] errorMessage = new string[] { DateTime.Now.ToString(), errorType, message, location };
            File.AppendAllLines(path, errorMessage);
        }
        public List<MessageDO> ReadLogs()
        {
            List<MessageDO> logs = new List<MessageDO>();
            string path = @"E:\CatSim (Capstone)\ApplicationLogger.txt";
            string[] log = File.ReadAllLines(path);
            for (int index = 0; index < log.Length; index++)
            {
                MessageDO mes = new MessageDO();
                mes.DateTime = log[index];
                index++;
                mes.ErrorType = log[index];
                index++;
                mes.ErrorMessage = log[index];
                index++;
                mes.Layer = log[index];
                logs.Add(mes);
            }
            return logs;
        }
    }
}
