﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
   public class EndingsDM
    {
        public int ID { get; set; }
        public string Fight { get; set; }
        public string Play { get; set; }
        public string Generic { get; set; }
    }
}
