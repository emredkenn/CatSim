﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public interface ICatDAO
    {
        void CreateCat(AllCatDM meow);
        void DeleteCat(AllCatDM meow);
        void UpdateCat(CatDM meow);
        List<CatDM> ReadCats(SqlParameter[] parameters, string statement);
        CatDM GetCatByID(int id);
    }
}
