﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class AllCatDM
    {
        public CatDM Meow { get; set; }
        public CatInfoDM Bio { get; set; }
        public StatsDM Stats { get; set; }
        public EndingsDM Ending { get; set; }
    }
}
