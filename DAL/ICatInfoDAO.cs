﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public interface ICatInfoDAO
    {
        void CreateCatInfo(CatInfoDM meow);
        void DeleteCatInfo(CatInfoDM meow);
        void UpdateCatInfo(CatInfoDM meow);
        List<CatInfoDM> ReadCatInfo(SqlParameter[] parameters, string statement);
        CatInfoDM GetCatInfoByID(int id);
    }
}
