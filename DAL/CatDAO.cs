﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using ApplicationLogger;


namespace DAL
{
    public class CatDAO : ICatDAO
    {
        public string ConnectionString = @"Server=.\SQLEXPRESS; Database = CatSim; Trusted_Connection = True;";

        private IDAO dataWriter;
        private ILogger logger;

        public CatDAO(IDAO data, ILogger log)
        {
            dataWriter = data;
            logger = log;
        }

        public void CreateCat(AllCatDM meow)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@Name", meow.Meow.Name)
                ,new SqlParameter("@StatsID", meow.Meow.StatsID)
                ,new SqlParameter("@CatInfoID", meow.Meow.CatInfoID)
                ,new SqlParameter ("@Fight", meow.Ending.Fight)
                ,new SqlParameter ("@Play", meow.Ending.Play)
                ,new SqlParameter ("@Generic", meow.Ending.Generic)
                ,new SqlParameter ("@Bio", meow.Bio.Bio)
                ,new SqlParameter ("@EndingsID", meow.Bio.EndingsID)
                ,new SqlParameter ("@Friendliness", meow.Stats.Friendliness)
                ,new SqlParameter ("@Aggressiveness", meow.Stats.Aggressiveness)
            };
                //DAO dataWriter = new DAO();
                dataWriter.Write(parameters, "CreateCat");
                logger.LogError("Event", "Method was able to create a new Cat in the database.", "Class:CatDAO Method:CreateCat");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to create a new Cat in the database.", "Class:CatDAO Method:CreateCat");
            }
        }
        public void DeleteCat(AllCatDM meow)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("ID", meow.Meow.ID)
                ,new SqlParameter ("ID", meow.Ending.ID)
                ,new SqlParameter ("ID", meow.Bio.ID)
                ,new SqlParameter ("ID", meow.Stats.ID)
            };
                dataWriter.Write(parameters, "DeleteCat");
                logger.LogError("Event", "Method was able to delete a Cat in the database.", "Class:CatDAO Method:DeleteCat");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to delete a Cat in the database.", "Class:CatDAO Method:DeleteCat");
            }
        }
        public CatDM GetCatByID(int id)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter ("@ID", id) };
                logger.LogError("Event", "Method was able to get a Cat by ID from the database.", "Class:CatDAO Method:GetCatByID");
                return ReadCats(parameters, "GetCatByID").SingleOrDefault();
            }
            catch
            {
                logger.LogError("Error", "Method was not able to get a Cat by ID from the database.", "Class:CatDAO Method:GetCatByID");
                return null;
            }
        }
        public List<CatDM> ReadCats(SqlParameter[] parameters, string statement)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(statement, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    connection.Open();
                    SqlDataReader data = command.ExecuteReader();
                    List<CatDM> kitties = new List<CatDM>();
                    while (data.Read())
                    {
                        CatDM meow = new CatDM();
                        meow.ID = (int)data["ID"];
                        meow.Name = data["Name"].ToString();
                        meow.StatsID = (int)data["StatsID"];
                        meow.CatInfoID = (int)data["CatInfoID"];
                        kitties.Add(meow);
                    }
                    return kitties;
                }
            }
        }
        public void UpdateCat(CatDM meow)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[]{
                 new SqlParameter("@ID",meow.ID)
                ,new SqlParameter("@Name", meow.Name)
                ,new SqlParameter("@StatsID", meow.StatsID)
                ,new SqlParameter("@CatInfoID", meow.CatInfoID)
            };
                dataWriter.Write(parameters, "UpdateCat");
                logger.LogError("Event", "Method was able to update a Cat in the database.", "Class:CatDAO Method:UpdateCat");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to update a Cat in the database.", "Class:CatDAO Method:UpdateCat");
            }
        }
    }
}
