﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public interface IStatsDAO
    {
        void CreateStats(StatsDM statistic);
        void DeleteStats(StatsDM statistic);
        void UpdateStats(StatsDM statistic);
        List<StatsDM> ReadStats(SqlParameter[] parameters, string statement);
        StatsDM GetStatsByID(int id);
    }
}
