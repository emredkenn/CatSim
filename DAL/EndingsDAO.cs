﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using ApplicationLogger;

namespace DAL
{
    public class EndingsDAO : IEndingsDAO
    {
        public string ConnectionString = @"Server=.\SQLEXPRESS; Database = CatSim; Trusted_Connection = True;";

        private IDAO dataWriter;
        private ILogger logger;

        public EndingsDAO(IDAO data, ILogger log)
        {
            dataWriter = data;
            logger = log;
        }

        public void CreateEnding(EndingsDM scene)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@Fight", scene.Fight)
                ,new SqlParameter("@Play", scene.Play)
                ,new SqlParameter("@Generic", scene.Generic)
            };
                dataWriter.Write(parameters, "CreateEnding");
                logger.LogError("Event", "Method was able to create a new Ending in the database.", "Class:EndingsDAO Method:CreateEnding");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to create a new Ending in the database.", "Class:EndingsDAO Method:CreateEnding");
            }
        }
        public void DeleteEnding(EndingsDM scene)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("ID",scene.ID)
            };
                dataWriter.Write(parameters, "DeleteEnding");
                logger.LogError("Event", "Method was able to delete an Ending in the database.", "Class:EndingsDAO Method:DeleteEnding");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to delete an Ending in the database.", "Class:EndingsDAO Method:DeleteEnding");
            }
        }
        public void UpdateEnding(EndingsDM scene)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[]{
                 new SqlParameter("@ID",scene.ID)
                 ,new SqlParameter("@Fight", scene.Fight)
                ,new SqlParameter("@Play", scene.Play)
                ,new SqlParameter("@Generic", scene.Generic)
            };
                dataWriter.Write(parameters, "UpdateEnding");
                logger.LogError("Event", "Method was able to update an Ending in the database.", "Class:EndingsDAO Method:UpdateEnding");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to update an Ending in the database.", "Class:EndingsDAO Method:UpdateEnding");
            }
        }
        public List<EndingsDM> ReadEndings(SqlParameter[] parameters, string statement)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(statement, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    connection.Open();
                    SqlDataReader data = command.ExecuteReader();
                    List<EndingsDM> scenarios = new List<EndingsDM>();
                    while (data.Read())
                    {
                        EndingsDM scene = new EndingsDM();
                        scene.ID = (int)data["ID"];
                        scene.Fight = data["Fight"].ToString();
                        scene.Play = data["Play"].ToString();
                        scene.Generic = data["Generic"].ToString();
                        scenarios.Add(scene);
                    }
                    return scenarios;
                }
            }
        }
        public EndingsDM GetEndingByID(int id)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter ("@ID", id) };
                logger.LogError("Event", "Method was able to get an Ending by ID from the database.", "Class:EndingsDAO Method:GetEndingByID");
                return ReadEndings(parameters, "GetEndingByID").SingleOrDefault();
            }
            catch
            {
                logger.LogError("Error", "Method was not able to get an Ending by ID from the database.", "Class:EndingsDAO Method:GetEndingByID");
                return null;
            }
        }
        public EndingsDM GetFightEndingByID(int id)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[]{
               new SqlParameter ("@ID", id) };
                logger.LogError("Event", "Method was able to get a FightEnding by ID from the database.", "Class:EndingsDAO Method:GetFightEndingByID");
                return ReadEndings(parameters, "GetFightEndingByID").SingleOrDefault();
            }
            catch
            {
                logger.LogError("Error", "Method was not able to get a FightEnding by ID from the database.", "Class:EndingsDAO Method:GetFightEndingByID");
                return null;
            }
        }
        public EndingsDM GetPlayEndingByID(int id)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter ("@ID", id) };
                logger.LogError("Event", "Method was able to get a PlayEnding by ID from the database.", "Class:EndingsDAO Method:GetPlayEndingByID");
                return ReadEndings(parameters, "GetPlayEndingByID").SingleOrDefault();
            }
            catch
            {
                logger.LogError("Error", "Method was not able to get a PlayEnding by ID from the database.", "Class:EndingsDAO Method:GetPlayEndingByID");
                return null;
            }
        }
        public EndingsDM GetGenericEndingByID(int id)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[]{
               new SqlParameter ("@ID", id) };
                logger.LogError("Event", "Method was able to get a GenericEnding by ID from the database.", "Class:EndingsDAO Method:GetGenericEndingByID");
                return ReadEndings(parameters, "GetGenericEndingByID").SingleOrDefault();
            }
            catch
            {
                logger.LogError("Error", "Method was not able to get a GenericEnding by ID from the database.", "Class:EndingsDAO Method:GetGenericEndingByID");
                return null;
            }
        }
    }
}
