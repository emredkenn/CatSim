﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using ApplicationLogger;

namespace DAL
{
    public class CatInfoDAO : ICatInfoDAO
    {
        public string ConnectionString = @"Server=.\SQLEXPRESS; Database = CatSim; Trusted_Connection = True;";

        private IDAO dataWriter;
        private ILogger logger;

        public CatInfoDAO(IDAO data, ILogger log)
        {
            dataWriter = data;
            logger = log;
        }

        public void CreateCatInfo(CatInfoDM meow)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@Bio", meow.Bio)
                ,new SqlParameter("@EndingsID", meow.EndingsID)
            };
                dataWriter.Write(parameters, "CreateCatInfo");
                logger.LogError("Event", "Method was able to create a new CatInfo in the database.", "Class:CatInfoDAO Method:CreateCatInfo");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to create a new CatInfo in the database.", "Class:CatInfoDAO Method:CreateCatInfo");
            }
        }
        public void DeleteCatInfo(CatInfoDM meow)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("ID",meow.ID)
            };
                dataWriter.Write(parameters, "DeleteCatInfo");
                logger.LogError("Event", "Method was able to delete a CatInfo in the database.", "Class:CatInfoDAO Method:DeleteCatInfo");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to delete a CatInfo in the database.", "Class:CatInfoDAO Method:DeleteCatInfo");
            }
        }
        public void UpdateCatInfo(CatInfoDM meow)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[]{
                 new SqlParameter("@ID",meow.ID)
                ,new SqlParameter("@Bio", meow.Bio)
                ,new SqlParameter("@EndingsID", meow.EndingsID)
            };
                dataWriter.Write(parameters, "UpdateCatInfo");
                logger.LogError("Event", "Method was able to update a CatInfo in the database.", "Class:CatDAO Method:UpdateCatInfo");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to update a CatInfo in the database.", "Class:CatInfoDAO Method:UpdateCatInfo");
            }
        }
        public List<CatInfoDM> ReadCatInfo(SqlParameter[] parameters, string statement)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(statement, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    connection.Open();
                    SqlDataReader data = command.ExecuteReader();
                    List<CatInfoDM> log = new List<CatInfoDM>();
                    while (data.Read())
                    {
                        CatInfoDM info = new CatInfoDM();
                        info.ID = (int)data["ID"];
                        info.Bio = data["Bio"].ToString();
                        info.EndingsID = (int)data["EndingsID"];
                        log.Add(info);
                    }
                    return log;
                }
            }
        }
        public CatInfoDM GetCatInfoByID(int id)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter ("@ID", id) };
                logger.LogError("Event", "Method was able to get a CatInfo by ID from the database.", "Class:CatInfoDAO Method:GetCatInfoByID");
                return ReadCatInfo(parameters, "GetCatInfoByID").SingleOrDefault();
            }
            catch
            {
                logger.LogError("Error", "Method was not able to get a CatInfo by ID from the database.", "Class:CatInfoDAO Method:GetCatInfoByID");
                return null;
            }
        }
    }
}
