﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class CatInfoDM
    {
        public int ID { get; set; }
        public string Bio { get; set; }
        public int EndingsID { get; set; }
    }
}
