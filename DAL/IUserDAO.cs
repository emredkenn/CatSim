﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public interface IUserDAO
    {
        void Register(UserDM account);
        void DeleteUser(UserDM account);
        void UpdateUser(UserDM account);
        List<UserDM> ReadUsers(SqlParameter[] parameters, string statement);
        UserDM GetUserByID(int id);
    }
}
