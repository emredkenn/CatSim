﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using ApplicationLogger;

namespace DAL
{
    public class UserDAO : IUserDAO
    {
        public string ConnectionString = @"Server=.\SQLEXPRESS; Database = CatSim; Trusted_Connection = True;";

        private IDAO dataWriter;
        private ILogger logger;

        public UserDAO(IDAO data, ILogger log)
        {
            dataWriter = data;
            logger = log;
        }

        public void Register(UserDM account)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@Username",account.Username)
                ,new SqlParameter("@Password",account.Password)
                ,new SqlParameter("@Role",account.Role)
            };
                dataWriter.Write(parameters, "CreateUser");
                logger.LogError("Event", "Method was able to register a new User in the database.", "Class:UserDAO Method:Register");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to register a new User in the database.", "Class:UserDAO Method:Register");
            }
        }
        public void DeleteUser(UserDM account)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("ID",account.ID)
            };
                dataWriter.Write(parameters, "DeleteUser");
                logger.LogError("Event", "Method was able to delete a User in the database.", "Class:UserDAO Method:DeleteUser");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to delete a User in the database.", "Class:UserDAO Method:DeleteUser");
            }
        }
        public UserDM GetUserByID(int id)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter ("@ID", id) };
                logger.LogError("Event", "Method was able to get a User by ID from the database.", "Class:UserDAO Method:GetUserByID");
                return ReadUsers(parameters, "GetUserByID").SingleOrDefault();
            }
            catch
            {
                logger.LogError("Error", "Method was not able to get a User by ID from the database.", "Class:UserDAO Method:GetUserByID");
                return null;
            }
        }
        public List<UserDM> ReadUsers(SqlParameter[] parameters, string statement)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(statement, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    connection.Open();
                    SqlDataReader data = command.ExecuteReader();
                    List<UserDM> accounts = new List<UserDM>();
                    while (data.Read())
                    {
                        UserDM account = new UserDM();
                        account.ID = (int)data["ID"];
                        account.Username = data["Username"].ToString();
                        account.Password = data["Password"].ToString();
                        account.Role = data["Role"].ToString();
                        accounts.Add(account);
                    }
                    return accounts;
                }
            }
        }
        public void UpdateUser(UserDM account)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@ID",account.ID)
                ,new SqlParameter("@Username",account.Username)
                ,new SqlParameter("@Password",account.Password)
                ,new SqlParameter("@Role",account.Role)
            };
                dataWriter.Write(parameters, "UpdateUser");
                logger.LogError("Event", "Method was able to update a User in the database.", "Class:UserDAO Method:UpdateUser");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to update a User in the database.", "Class:UserDAO Method:UpdateUser");
            }
        }
    }
}
