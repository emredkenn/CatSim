﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public interface IEndingsDAO
    {
        void CreateEnding(EndingsDM scene);
        void DeleteEnding(EndingsDM scene);
        void UpdateEnding(EndingsDM scene);
        List<EndingsDM> ReadEndings(SqlParameter[] parameters, string statement);
        EndingsDM GetEndingByID(int id);
        EndingsDM GetFightEndingByID(int id);
        EndingsDM GetPlayEndingByID(int id);
        EndingsDM GetGenericEndingByID(int id);
    }
}
