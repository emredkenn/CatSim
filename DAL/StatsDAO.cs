﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using ApplicationLogger;

namespace DAL
{
    public class StatsDAO : IStatsDAO
    {
        public string ConnectionString = @"Server=.\SQLEXPRESS; Database = CatSim; Trusted_Connection = True;";

        private IDAO dataWriter;
        private ILogger logger;

        public StatsDAO(IDAO data, ILogger log)
        {
            dataWriter = data;
            logger = log;
        }

        public void CreateStats(StatsDM statistic)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@Friendliness", statistic.Friendliness)
                ,new SqlParameter("@Aggressiveness", statistic.Aggressiveness)
            };
                dataWriter.Write(parameters, "CreateStats");
                logger.LogError("Event", "Method was able to create a new Stats in the database.", "Class:StatsDAO Method:CreateStats");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to create a new Stats in the database.", "Class:StatsDAO Method:CreateStats");
            }
        }
        public void DeleteStats(StatsDM statistic)
        { 
            try
            {
                SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("ID",statistic.ID)
            };
                dataWriter.Write(parameters, "DeleteStats");
                logger.LogError("Event", "Method was able to delete a Stats in the database.", "Class:StatsDAO Method:DeleteStats");
            }
            catch
            { 
                logger.LogError("Error", "Method was not able to delete a Stats in the database.", "Class:StatsDAO Method:DeleteStats");
            }
        }
        public void UpdateStats(StatsDM statistic)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[]{
                 new SqlParameter("@ID",statistic.ID)
                ,new SqlParameter("@Friendliness", statistic.Friendliness)
                ,new SqlParameter("@Aggressiveness", statistic.Aggressiveness)
            };
                dataWriter.Write(parameters, "UpdateStats");
                logger.LogError("Event", "Method was able to update a Stats in the database.", "Class:StatsDAO Method:UpdateStats");
            }
            catch
            {
                logger.LogError("Error", "Method was not able to update a Stats in the database.", "Class:StatsDAO Method:UpdateStats");
            }
        }
        public List<StatsDM> ReadStats(SqlParameter[] parameters, string statement)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(statement, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    connection.Open();
                    SqlDataReader data = command.ExecuteReader();
                    List<StatsDM> levels = new List<StatsDM>();
                    while (data.Read())
                    {
                        StatsDM statistic = new StatsDM();
                        statistic.ID = (int)data["ID"];
                        statistic.Friendliness = (int)data["Friendliness"];
                        statistic.Aggressiveness = (int)data["Aggressiveness"];
                        levels.Add(statistic);
                    }
                    return levels;
                }
            }
        }
        public StatsDM GetStatsByID(int id)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter ("@ID", id) };
                logger.LogError("Event", "Method was able to get a Stats by ID from the database.", "Class:StatsDAO Method:GetStatsByID");
                return ReadStats(parameters, "GetStatsByID").SingleOrDefault();
            }
            catch
            {
                logger.LogError("Error", "Method was not able to get a Stats by ID from the database.", "Class:StatsDAO Method:GetStatsByID");
                return null;
            }
        }
    }
}